# Project 5: Brevet time calculator with Ajax and MongoDB
## Author: Bethany Van Meter, bvanmet2@uoregon.edu

Simple list of controle times from project 4 stored in MongoDB database.
Credits to Michal Young for the initial version of this code (at least in project 4).

## Functionality I added

Reused my code from project 4 (https://bitbucket.org/bethanyvanmeter/proj4-brevets/). Recall: created a list of open and close controle times using AJAX. In this project, the following functionality was created:
1) Create two buttons ("Submit") and ("Display") in the page where have controle times. 
2) On clicking the Submit button, the control times should be entered into the database.
3) On clicking the Display button, the entries from the database should be displayed in a new page. 

Handle error cases appropriately. Submit returns an error page if there are no controle times. 

## To run:

This will run on port 5000. 
In my browser, I will see my running application (if running locally): `localhost:5000`.

* `cd DockerMongo/`
* `docker-compose up`

If running locally: go to `localhost:5000` to see the application

## ACP controle times
That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.

The algorithm for calculating controle times is described here <https://rusa.org/pages/acp-brevet-control-times-calculator>. Additional background information is given here <https://rusa.org/pages/rulesForRiders>. 

Essentially replacing the calculator here <https://rusa.org/octime_acp.html>.

## The Calculator Rules:

Rules are based on the table below:

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr)  |
| --------------------- | --------------------- | ---------------------- |
| 0 - 200               | 15                    | 34                     |
| 200 - 400             | 15                    | 32                     |
| 400 - 600             | 15                    | 30                     |
| 600 - 1000            | 11.428                | 28                     |
| 1000 - 1300           | 13.333                | 26                     |

The calculation of a control's opening time is based on the maximum speed. Calculation of a control's closing time is based on the minimum speed.

* If a brevet equal to or below 60km, the close times are determined by 20km/hour plus 1 hour.
* If a control location distance is greater than the brevet, the brevet distance will be used to calculate the open and close times.
* The max closing times for each brevet length are predetermined (in hours and minutes, HH:MM):
	* 13:30 for 200 KM
	* 20:00 for 300 KM
	* 27:00 for 400 KM
	* 40:00 for 600 KM
	* and 75:00 for 1000 KM
		* In this case, if a brevit is 200km, and the control location is 200km, we won't calculate with the normal calculator, but by using the time limit above (so: 13:30 rather than 13:20).
* If a control point of 0 is given (the start), the closing time is 1 hour after the starting time. 
* Control point open and close times are calculated incrementally. For example: a 600km brevet distance with control points 150, 250, 350, 460, and 603 are determined by:
	* 150 -> within bracket 0-200, min speed = 15, max speed = 34
		* open time = 150 / 34
		* close time = 150 / 15
	* 250 -> within bracket 200-400 for 50 km (min speed = 15, max speed = 32), within bracket 0-200 for 200km (min speed = 15, max speed = 34)
		* open time = 200 / 34 + 50 / 32
		* close time = 200 / 15 + 50 / 15
	* 350 -> within bracket 200-400 for 150km (min speed = 15, max speed = 32), within bracket 0-200 for 200km (min speed = 15, max speed = 34)
		* open time = 200 / 34 + 150 / 32
		* close time = 200 / 15 + 150 / 15
	* 460 -> within bracket 400-600 for 60km (min speed = 15, max speed = 30), within bracket 200-400 for 200km (min speed = 15, max speed = 32), within bracket 0-200 for 200km (min speed = 15, max speed = 34)
		* open time = 200 / 34 + 200 / 32 + 60 / 30
		* close time = 200 / 15 + 200 / 15 + 60 / 15
	* 603 -> over the brevet length of 600, so calculate close time using max time for 600 and calculate open time using 600: within bracket 400-600 for 200km (min speed = 15, max speed = 30), within bracket 200-400 for 200km (min speed = 15, max speed = 32), within bracket 0-200 for 200km (min speed = 15, max speed = 34)
		* open time = 200 / 34 + 200 / 32 + 200 / 30
		* close time = 200 / 15 + 200 / 15 + 200 / 15