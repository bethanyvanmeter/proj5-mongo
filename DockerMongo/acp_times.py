"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# this is styled as control_distance: (min_speed, max_speed) all in km/km per hour
ALL_DISTANCE_TIMES = [
  (200, (15, 34)),
  (400, (15, 32)),
  (600, (15, 30)),
  (1000, (11.428, 28)),
  (1300, (13.333, 26)),
  ]

# in minutes
MAX_BREVIT_FINISH_TIMES = {
  200: 810,
  300: 1200,
  400: 1620,
  600: 2400,
  1000: 4500,
}

BREVIT_DIST = [200, 300, 400, 600, 1000]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_time = arrow.get(brevet_start_time)

    # if the control point is greater than or equal to the brevit length, 
    # just include the max brevit time
    if control_dist_km >= brevet_dist_km:
        control_dist_km = brevet_dist_km

    total_time = 0
    leftover = control_dist_km 
    previous_distance = 0
    for i in ALL_DISTANCE_TIMES:
      speed = i[1][1]
      if control_dist_km - i[0] >= 0:
        if i[0] != 200:
          to_calc = i[0] - ALL_DISTANCE_TIMES[previous_distance][0]
          previous_distance += 1
        else:
          to_calc = i[0]
        leftover = leftover - to_calc
        total_time += round(((to_calc / speed) * 60), 0)
      else:
        to_calc = leftover
        total_time += round(((to_calc / speed) * 60), 0)
        break

    brevet_time = brevet_time.shift(minutes=total_time)
    return brevet_time.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_time = arrow.get(brevet_start_time)

    # if the control point is greater than or equal to the brevit length, 
    # just include the max brevit time
    if control_dist_km < 60:
      speed = 20
      total_time = round(((control_dist_km / speed) * 60), 0)
      total_time += 60
      return brevet_time.shift(minutes=total_time).isoformat()

    if control_dist_km == 0:
      return brevet_time.shift(hours=1).isoformat()

    if control_dist_km >= brevet_dist_km:
        min_add = MAX_BREVIT_FINISH_TIMES[brevet_dist_km]
        return brevet_time.shift(minutes=min_add).isoformat()

    total_time = 0
    leftover = control_dist_km 
    previous_distance = 0
    for i in ALL_DISTANCE_TIMES:
      speed = i[1][0]
      if control_dist_km - i[0] >= 0:
        if i[0] != 200:
          to_calc = i[0] - ALL_DISTANCE_TIMES[previous_distance][0]
          previous_distance += 1
        else:
          to_calc = i[0]
        leftover = leftover - to_calc
        total_time += round(((to_calc / speed) * 60), 0)
      else:
        to_calc = leftover
        total_time += round(((to_calc / speed) * 60), 0)
        break

    brevet_time = brevet_time.shift(minutes=total_time)
    return brevet_time.isoformat()
