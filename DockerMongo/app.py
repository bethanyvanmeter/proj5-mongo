import os
from flask import Flask, redirect, url_for, request, render_template
import flask
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations

import logging

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/index")
@app.route('/')
def index():
    return render_template('calc.html')

@app.route('/todo')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    app.logger.debug("items: {}\n".format(items))

    if items == []:
        return flask.render_template('no_inputs.html')

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    # delete whatever was there before
    db.tododb.delete_many({})

    miles = request.form.getlist('miles[]')
    km = request.form.getlist('km[]')
    location = request.form.getlist('location[]')
    open_time = request.form.getlist('open[]')
    open_time = list(filter(None, open_time))
    close_time = request.form.getlist('close[]')
    if(len(open_time) < 1):
        return flask.render_template('no_inputs.html')

    for i in range(len(open_time)):
        item_doc = {
            'miles': miles[i],
            'km': km[i],
            'location': location[i],
            'open': open_time[i],
            'close': close_time[i],
        }
        db.tododb.insert_one(item_doc)

    return '', 204

@app.route("/no_inputs")
def no_inputs():
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('no_inputs.html'), 400

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevit_distance = request.args.get('brevit_distance', 1100, type=float)
    start_time = request.args.get('start_time')
    start_date = request.args.get('start_date')
    datetime = format_datetimes(start_date, start_time)

    app.logger.debug("km={}\n".format(km))
    app.logger.debug("brevit_distance={}\n".format(brevit_distance))
    app.logger.debug("request.args: {}\n".format(request.args))
    app.logger.debug("start time: {}\n".format(start_time))
    app.logger.debug("start date: {}\n".format(start_date))
    app.logger.debug("datetime: {}\n".format(datetime))
    
    open_time = acp_times.open_time(km, brevit_distance, datetime.isoformat())
    close_time = acp_times.close_time(km, brevit_distance, datetime.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

def format_datetimes(date, time):
    '''
    returns arrow object of date and time
    '''
    ar_obj = arrow.get(date + ' ' + time, 'YYYY-MM-DD HH:mm')
    return ar_obj

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
